class CreateElemento1s < ActiveRecord::Migration
  def change
    create_table :elemento1s do |t|
      t.string :nombre
      t.string :descripcion
      t.string :imagen
      t.integer :precio
      t.text :caracteristicas
      t.integer :tipo_id

      t.timestamps
    end
  end
end
